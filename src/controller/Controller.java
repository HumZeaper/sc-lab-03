package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.Model;
import view.View;

public class Controller {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}

	public Controller() {
		frame = new View();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		String url1 = "www.google.com";
		Model Model1 = new Model();
		Model1.setName(url1);
		int code1 = Model1.numberAscii(url1);
		int modnumber1 = Model1.modAscii(code1);
		frame.setResult("1. "+Model1.toString()+ " : " +code1+ " = "+modnumber1);
		
		String url2 = "www.facebooks.com";
		Model Model2 = new Model();
		Model2.setName(url2);
		int code2 = Model2.numberAscii(url2);
		int modnumber2 = Model2.modAscii(code2);
		frame.extendResult("2. "+Model2.toString()+ " : " +code2+ " = "+modnumber2);
		

		String url3 = "www.niceoppaia.net";
		Model Model3 = new Model();
		Model3.setName(url3);
		int code3 = Model3.numberAscii(url3);
		int modnumber3 = Model3.modAscii(code3);
		frame.extendResult("3. "+Model3.toString()+ " : " +code3+ " = "+modnumber3);
		
		String url4 = "www.honj.in.th";
		Model Model4 = new Model();
		Model4.setName(url4);
		int code4 = Model3.numberAscii(url4);
		int modnumber4 = Model4.modAscii(code4);
		frame.extendResult("4. "+Model4.toString()+ " : " +code4+ " = "+modnumber4);
		
		String url5 = "www.playlolqq.in.th";
		Model Model5 = new Model();
		Model5.setName(url5);
		int code5 = Model5.numberAscii(url5);
		int modnumber5 = Model5.modAscii(code5);
		frame.extendResult("5. "+Model5.toString()+ " : " +code5+ " = "+modnumber5);
		
		String url6 = "www.go-chand.blogspot.com";
		Model Model6 = new Model();
		Model6.setName(url6);
		int code6 = Model6.numberAscii(url6);
		int modnumber6 = Model6.modAscii(code6);
		frame.extendResult("6. "+Model6.toString()+ " : " +code6+ " = "+modnumber6);
		
		String url7 = "www.youtube.com";
		Model Model7 = new Model();
		Model7.setName(url7);
		int code7 = Model7.numberAscii(url7);
		int modnumber7 = Model7.modAscii(code7);
		frame.extendResult("7. "+Model7.toString()+ " : " +code7+ " = "+modnumber7);
		
		String url8 = "www.youlikew.com";
		Model Model8 = new Model();
		Model8.setName(url8);
		int code8 = Model8.numberAscii(url8);
		int modnumber8 = Model8.modAscii(code8);
		frame.extendResult("8. "+Model8.toString()+ " : " +code8+ " = "+modnumber8);
		
		String url9 = "www.nutzeapers.com";
		Model Model9 = new Model();
		Model9.setName(url9);
		int code9 = Model9.numberAscii(url9);
		int modnumber9 = Model9.modAscii(code9);
		frame.extendResult("9. "+Model9.toString()+ " : " +code9+ " = "+modnumber9);
		
		String url10 = "www.youloveww.com";
		Model Model10 = new Model();
		Model10.setName(url10);
		int code10 = Model10.numberAscii(url10);
		int modnumber10 = Model10.modAscii(code10);
		frame.extendResult("10. "+Model10.toString()+ " : " +code10+ " = "+modnumber10);
		
		String url11 = "www.loveer.com";
		Model Model11 = new Model();
		Model11.setName(url11);
		int code11 = Model11.numberAscii(url11);
		int modnumber11 = Model11.modAscii(code11);
		frame.extendResult("11. "+Model11.toString()+ " : " +code11+ " = "+modnumber11);
		
		String url12 = "www.nuthum.com";
		Model Model12 = new Model();
		Model12.setName(url12);
		int code12 = Model12.numberAscii(url12);
		int modnumber12 = Model12.modAscii(code12);
		frame.extendResult("12. "+Model12.toString()+ " : " +code12+ " = "+modnumber12);
		
		
		
		
		
		
		
		
		
	

	}

	ActionListener list;
	View frame;
}
